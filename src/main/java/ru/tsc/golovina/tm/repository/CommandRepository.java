package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.ICommandRepository;
import ru.tsc.golovina.tm.command.AbstractCommand;

import java.util.*;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        return arguments.get(arg);
    }

    @NotNull
    @Override
    public Collection<String> getCommandNames() {
        @NotNull final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            @NotNull final String name = command.getCommand();
            if (name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<String> getArgNames() {
        @NotNull final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : arguments.values()) {
            @Nullable final String arg = command.getArgument();
            if (!Optional.ofNullable(arg).isPresent() || arg.isEmpty()) continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String arg = command.getArgument();
        @NotNull final String name = command.getCommand();
        if (Optional.ofNullable(arg).isPresent()) arguments.put(arg, command);
        commands.put(name, command);
    }

}