package ru.tsc.golovina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    E add(@NotNull String userId, @NotNull E entity);

    @Nullable
    E findById(@NotNull String userId, @NotNull String id);

    @NotNull
    E findByIndex(@NotNull String userId, @NotNull Integer index);

    void clear(@NotNull String userId);

    @Nullable
    E removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeByIndex(@NotNull String userId, @NotNull Integer index);

    void remove(@NotNull String userId, @NotNull E entity);

    @NotNull
    Integer getSize(@NotNull String userId);

}
