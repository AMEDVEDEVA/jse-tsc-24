package ru.tsc.golovina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IService;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    void setPassword(@Nullable String id, @Nullable String password);

    void setRole(@Nullable String userId, @Nullable Role role);

    @NotNull
    User findUserByLogin(@Nullable String login);

    @NotNull
    User findUserByEmail(@Nullable String email);

    void removeUserById(@Nullable String id);

    @NotNull
    User removeUserByLogin(@NotNull String login);

    boolean isLoginExists(@NotNull String login);

    boolean isEmailExists(@NotNull String email);

    void updateUserById(@Nullable String id, @Nullable String lastName, @Nullable String firstName,
                        @Nullable String middleName, @Nullable String email);

    void updateUserByLogin(@Nullable String login, @Nullable String lastName, @Nullable String firstName,
                           @Nullable String middleName, @Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
