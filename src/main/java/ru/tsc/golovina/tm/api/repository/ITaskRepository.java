package ru.tsc.golovina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    @NotNull
    List<Task> findAllTaskByProjectId(String userId, String projectId);

    boolean existsByName(@NotNull String userId, @NotNull String name);

    void bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void unbindTaskById(@NotNull String userId, @NotNull String taskId);

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    Task findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task startById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task startByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task startByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task finishById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task finishByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task finishByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @Nullable
    Task changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @Nullable
    Task changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

}