package ru.tsc.golovina.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.golovina.tm.api.repository.*;
import ru.tsc.golovina.tm.api.service.*;
import ru.tsc.golovina.tm.command.AbstractCommand;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.system.UnknownCommandException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.repository.*;
import ru.tsc.golovina.tm.service.*;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Optional;
import java.util.Set;

@Getter
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService);

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.golovina.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.tsc.golovina.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            try {
                registry(clazz.newInstance());
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }


    public void start(@Nullable final String... args) {
        displayWelcome();
        runArgs(args);
        initData();
        initUsers();
        initCommands();
        logService.debug("Test environment");
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                @NotNull final String command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

    private void initData() {
        projectService.add(new Project("Project 3", "-"));
        projectService.add(new Project("Project 2", "-"));
        projectService.add(new Project("Project 1", "-"));
        projectService.add(new Project("Project 4", "-"));
        taskService.add(new Task("Task 1", "-"));
        taskService.add(new Task("Task 4", "-"));
        taskService.add(new Task("Task 3", "-"));
        taskService.add(new Task("Task 2", "-"));
    }

    public void initUsers() {
        final User user = new User("user", "user");
        user.setEmail("user@email.ru");
        userService.add(user);
        final User admin = new User("admin", "admin");
        admin.setRole(Role.ADMIN);
        userService.add(admin);
    }


    private void displayWelcome() {
        System.out.println("---Welcome to task manager---");
    }

    private void runArgs(@Nullable final String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return;
        AbstractCommand command = commandService.getCommandByName(args[0]);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(args[0]);
        command.execute();
    }

    private void runCommand(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent()) throw new UnknownCommandException(command);
        final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    private void registry(@NotNull AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
