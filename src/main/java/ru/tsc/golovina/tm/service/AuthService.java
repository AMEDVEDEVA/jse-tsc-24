package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IAuthRepository;
import ru.tsc.golovina.tm.api.service.IAuthService;
import ru.tsc.golovina.tm.api.service.IUserService;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.empty.EmptyLoginException;
import ru.tsc.golovina.tm.exception.empty.EmptyPasswordException;
import ru.tsc.golovina.tm.exception.system.AccessDeniedException;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.util.HashUtil;

import java.util.Objects;

public final class AuthService implements IAuthService {

    @NotNull
    private final IAuthRepository authRepository;

    @NotNull
    private final IUserService userService;

    public AuthService(@NotNull final IAuthRepository authRepository, @NotNull final IUserService userService) {
        this.authRepository = authRepository;
        this.userService = userService;
    }

    @NotNull
    @Override
    public String getCurrentUserId() {
        @NotNull final String userId = authRepository.getCurrentUserId();
        return userId;
    }

    @Override
    public void setCurrentUserId(@Nullable final String userId) {
        if (userId == null) return;
        authRepository.setCurrentUserId(userId);
    }

    @Override
    public boolean isAuth() {
        @NotNull final String currentUserId = authRepository.getCurrentUserId();
        return !currentUserId.isEmpty();
    }

    @Override
    public boolean isAdmin() {
        @NotNull final String userId = getCurrentUserId();
        @NotNull final Role role = userService.findById(userId).getRole();
        return role.equals(Role.ADMIN);
    }

    @Override
    public void login(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findUserByLogin(login);
        if (user.getLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPassword())) throw new AccessDeniedException();
        setCurrentUserId(user.getId());
    }

    @Override
    public void logout() {
        if (!isAuth()) throw new AccessDeniedException();
        setCurrentUserId(null);
    }

    @Override
    public void checkRoles(@Nullable final Role... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final User user = userService.findById(getCurrentUserId());
        @Nullable final Role role = user.getRole();
        for (final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}