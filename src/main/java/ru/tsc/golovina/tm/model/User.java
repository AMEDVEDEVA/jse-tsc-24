package ru.tsc.golovina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.util.HashUtil;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String password;

    @Nullable
    private String email;

    @Nullable
    private String lastName;

    @Nullable
    private String firstName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USER;

    private Boolean locked = false;

    public User(@NotNull final String login, @NotNull final String password) {
        this.login = login;
        this.password = HashUtil.salt(password);
    }

}