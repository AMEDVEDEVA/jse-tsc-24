package ru.tsc.golovina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "about";
    }

    @Nullable
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display developer info";
    }

    @Override
    public void execute() {
        System.out.println("Developer: Alla Golovina");
        System.out.println("e-mail: amedvedeva@tsconsulting.com");
    }

}
