package ru.tsc.golovina.tm.command;

import lombok.NoArgsConstructor;
import ru.tsc.golovina.tm.api.service.ServiceLocator;
import ru.tsc.golovina.tm.enumerated.Role;

@NoArgsConstructor
public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    public abstract String getCommand();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract void execute();

    @Override
    public String toString() {
        String result = "";
        final String name = getCommand();
        final String arg = getArgument();
        final String description = getDescription();

        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "]";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
