package ru.tsc.golovina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractTaskCommand;
import ru.tsc.golovina.tm.enumerated.Role;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "task-clear";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Drop all tasks";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getCurrentUserId();
        serviceLocator.getTaskService().clear(userId);
    }

}
