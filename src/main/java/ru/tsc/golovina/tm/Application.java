package ru.tsc.golovina.tm;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}